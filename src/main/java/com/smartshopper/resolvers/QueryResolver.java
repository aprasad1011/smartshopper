package com.smartshopper.resolvers;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.smartshopper.model.User;
import com.smartshopper.service.UserService;

@Component
public class QueryResolver implements GraphQLQueryResolver {
	private UserService userService ;
	
	public QueryResolver(UserService userService) {
		this.userService = userService;
	}
	
	public List<User> getUsers(int first, int last) {
		System.out.println("Query: [" + first + "] to [" + last + "]");
		if((first == 0) && (last == 0)) {
			return this.userService.getAllUsers();
		} else {
			return this.userService.getUsers(first, last);
		}
	}
	
	public Optional<User> getUser(int id) {
		return this.userService.getUser(id);
	}
}

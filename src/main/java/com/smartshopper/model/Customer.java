package com.smartshopper.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CUSTOMER")
public class Customer {

	// Customer Id (auto-generated)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "ID", nullable = false)
	private int id;

	// Customer first name (login id)
	@Column(name = "FIRST_NAME", length = 32, nullable = false)
	private String firstName;

	// Customer last name (login id)
	@Column(name = "LAST_NAME", length = 32, nullable = false)
	private String lastName;

	// customer mobile number
	@Column(name = "CONTACT_NUMBER", length = 12, nullable = false)
	private String contactNumber;

	// customer email id
	@Column(name = "EMAIL_ID", length = 64)
	private String emailId;

	@Column(name = "ADDRESS", length = 200, nullable = false)
	private String address;
	
	@Column(name = "ENTITY", length = 200, nullable = false)
	private String entity;

	public int getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	
	
	
	
}

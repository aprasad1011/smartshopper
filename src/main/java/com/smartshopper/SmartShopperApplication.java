package com.smartshopper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartShopperApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartShopperApplication.class, args);
	}

	
}

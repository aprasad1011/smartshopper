package com.smartshopper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smartshopper.model.Customer;
import com.smartshopper.model.User;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}

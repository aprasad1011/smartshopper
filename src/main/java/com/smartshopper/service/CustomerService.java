package com.smartshopper.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smartshopper.model.Customer;
import com.smartshopper.repository.CustomerRepository;

@Service 
@Transactional
public class CustomerService {
	private CustomerRepository store ;
	
	public CustomerService(CustomerRepository db) {
		this.store = db ;
	}
	
	public List<Customer> getAllCustomers() {
		return this.store.findAll();
	}
	
	public Optional<Customer> getCustomer(int id) {
		return this.store.findById(id);
	}
	
	public List<Customer> getCustomers(int first, int last) {
		if ((last == 0) || (last < first)) {
			// Ignore last if invalid value was specified
			last = (int) this.store.count();
		}
		return this.store.findAllById(
			IntStream.rangeClosed(first, last)
			.boxed()
			.collect(Collectors.toList())
		);
	}
	
	public Customer newCustomer(String firstName, String lastName, String contactNumber, String emailId, String address, String entity) {
		Customer cust = new Customer();
		cust.setFirstName(firstName);
		cust.setLastName(lastName);
		cust.setContactNumber(contactNumber);
		cust.setEmailId(emailId);
		cust.setAddress(address);
		cust.setEntity(entity);
		return this.store.save(cust);
	}
	
	public Customer saveCustomer(Customer Customer) {
		return this.store.save(Customer);
	}
	
	public void deleteCustomer(int id) {
		this.store.deleteById(id);
	}
}

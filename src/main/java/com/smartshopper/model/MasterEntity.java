package com.smartshopper.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ENTITY")
public class MasterEntity {

	// entity Id (auto-generated)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private int id;

	// Entity code
	@Column(name = "ENTITY_CODE", length = 32, nullable = false)
	private String entityCode;

	// Entity description
	@Column(name = "DESCRIPTION", length = 64, nullable = false)
	private String description;

	// on boarded date for ref purpose
	@Column(name = "ON_BOARDED_DATE", nullable = false)
	private Date onBoardedDate;
	
	// IS_ACTIVE
	@Column(name = "IS_ACTIVE", nullable = false)
	private Date isActive;
	
	// next bill id based on entity
	@Column(name = "NEXT_BILL_ID")
	private int nextBillId;

}

Graphql UI:  http://localhost:9000/graphiql

H2 DB Console: http://localhost:9000/h2/ 

Create User Query: 

mutation{
  createUser(
    login : "krishna"
    name:"Krishna Lahoti"
  )
  {
    id
  }
}
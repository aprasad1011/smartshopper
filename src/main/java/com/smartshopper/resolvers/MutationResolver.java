package com.smartshopper.resolvers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.smartshopper.model.User;
import com.smartshopper.service.UserService;

@Component
public class MutationResolver implements GraphQLMutationResolver {
	
	@Autowired
	private UserService userService;

	public User createUser(String login, String name) {
		return this.userService.newUser(login, name);
	}
}

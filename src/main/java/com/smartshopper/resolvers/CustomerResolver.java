package com.smartshopper.resolvers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.smartshopper.model.Customer;
import com.smartshopper.service.CustomerService;

@Component
public class CustomerResolver implements GraphQLQueryResolver, GraphQLMutationResolver {
	@Autowired
	private CustomerService customerService;

	public List<Customer> getCustomers(int first, int last) {
		System.out.println("Query: [" + first + "] to [" + last + "]");
		if ((first == 0) && (last == 0)) {
			return this.customerService.getAllCustomers();
		} else {
			return this.customerService.getCustomers(first, last);
		}
	}

	public Optional<Customer> getCustomer(int id) {
		return this.customerService.getCustomer(id);
	}

	public Customer createCustomer(String firstName, String lastName, String contactNumber, String emailId, String address, String entity) {
		return this.customerService.newCustomer(firstName, lastName, contactNumber, emailId, address, entity);
	}

}
